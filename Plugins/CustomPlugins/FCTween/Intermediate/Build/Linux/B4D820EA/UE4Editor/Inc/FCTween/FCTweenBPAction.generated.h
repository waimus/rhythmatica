// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FCTWEEN_FCTweenBPAction_generated_h
#error "FCTweenBPAction.generated.h already included, missing '#pragma once' in FCTweenBPAction.h"
#endif
#define FCTWEEN_FCTweenBPAction_generated_h

#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_8_DELEGATE \
static inline void FTweenEventOutputPin_DelegateWrapper(const FMulticastScriptDelegate& TweenEventOutputPin) \
{ \
	TweenEventOutputPin.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetTimeMultiplier); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execRestart); \
	DECLARE_FUNCTION(execUnpause); \
	DECLARE_FUNCTION(execPause);


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetTimeMultiplier); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execRestart); \
	DECLARE_FUNCTION(execUnpause); \
	DECLARE_FUNCTION(execPause);


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFCTweenBPAction(); \
	friend struct Z_Construct_UClass_UFCTweenBPAction_Statics; \
public: \
	DECLARE_CLASS(UFCTweenBPAction, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenBPAction)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUFCTweenBPAction(); \
	friend struct Z_Construct_UClass_UFCTweenBPAction_Statics; \
public: \
	DECLARE_CLASS(UFCTweenBPAction, UBlueprintAsyncActionBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenBPAction)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenBPAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenBPAction) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenBPAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenBPAction); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenBPAction(UFCTweenBPAction&&); \
	NO_API UFCTweenBPAction(const UFCTweenBPAction&); \
public:


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenBPAction(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenBPAction(UFCTweenBPAction&&); \
	NO_API UFCTweenBPAction(const UFCTweenBPAction&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenBPAction); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenBPAction); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenBPAction)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_10_PROLOG
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_PRIVATE_PROPERTY_OFFSET \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_SPARSE_DATA \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_INCLASS_NO_PURE_DECLS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FCTWEEN_API UClass* StaticClass<class UFCTweenBPAction>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_Blueprints_FCTweenBPAction_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
