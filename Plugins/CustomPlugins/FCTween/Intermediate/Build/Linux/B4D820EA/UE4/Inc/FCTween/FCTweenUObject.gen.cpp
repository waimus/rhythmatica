// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FCTween/Public/FCTweenUObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFCTweenUObject() {}
// Cross Module References
	FCTWEEN_API UClass* Z_Construct_UClass_UFCTweenUObject_NoRegister();
	FCTWEEN_API UClass* Z_Construct_UClass_UFCTweenUObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_FCTween();
// End Cross Module References
	void UFCTweenUObject::StaticRegisterNativesUFCTweenUObject()
	{
	}
	UClass* Z_Construct_UClass_UFCTweenUObject_NoRegister()
	{
		return UFCTweenUObject::StaticClass();
	}
	struct Z_Construct_UClass_UFCTweenUObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFCTweenUObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FCTween,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFCTweenUObject_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * @brief Use this to wrap an FCTweenInstance inside a UObject, so that it's destroyed when its outer object is destroyed\n */" },
		{ "IncludePath", "FCTweenUObject.h" },
		{ "ModuleRelativePath", "Public/FCTweenUObject.h" },
		{ "ToolTip", "@brief Use this to wrap an FCTweenInstance inside a UObject, so that it's destroyed when its outer object is destroyed" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFCTweenUObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFCTweenUObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFCTweenUObject_Statics::ClassParams = {
		&UFCTweenUObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFCTweenUObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFCTweenUObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFCTweenUObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFCTweenUObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFCTweenUObject, 2041216559);
	template<> FCTWEEN_API UClass* StaticClass<UFCTweenUObject>()
	{
		return UFCTweenUObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFCTweenUObject(Z_Construct_UClass_UFCTweenUObject, &UFCTweenUObject::StaticClass, TEXT("/Script/FCTween"), TEXT("UFCTweenUObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFCTweenUObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
