// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FCTWEEN_FCTweenUObject_generated_h
#error "FCTweenUObject.generated.h already included, missing '#pragma once' in FCTweenUObject.h"
#endif
#define FCTWEEN_FCTweenUObject_generated_h

#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_SPARSE_DATA
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_RPC_WRAPPERS
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFCTweenUObject(); \
	friend struct Z_Construct_UClass_UFCTweenUObject_Statics; \
public: \
	DECLARE_CLASS(UFCTweenUObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenUObject)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUFCTweenUObject(); \
	friend struct Z_Construct_UClass_UFCTweenUObject_Statics; \
public: \
	DECLARE_CLASS(UFCTweenUObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FCTween"), NO_API) \
	DECLARE_SERIALIZER(UFCTweenUObject)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFCTweenUObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFCTweenUObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenUObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenUObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenUObject(UFCTweenUObject&&); \
	NO_API UFCTweenUObject(const UFCTweenUObject&); \
public:


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFCTweenUObject(UFCTweenUObject&&); \
	NO_API UFCTweenUObject(const UFCTweenUObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFCTweenUObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFCTweenUObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFCTweenUObject)


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_PRIVATE_PROPERTY_OFFSET
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_10_PROLOG
#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_PRIVATE_PROPERTY_OFFSET \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_SPARSE_DATA \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_RPC_WRAPPERS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_INCLASS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_PRIVATE_PROPERTY_OFFSET \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_SPARSE_DATA \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_INCLASS_NO_PURE_DECLS \
	Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FCTWEEN_API UClass* StaticClass<class UFCTweenUObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Rhythmatica_Plugins_CustomPlugins_FCTween_Source_FCTween_Public_FCTweenUObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
