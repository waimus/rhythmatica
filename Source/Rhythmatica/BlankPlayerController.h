// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BlankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RHYTHMATICA_API ABlankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	float WorldBpm;

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	void SetBpm(float bpm);
	
	
};
