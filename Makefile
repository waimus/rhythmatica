# Makefile generated by MakefileGenerator.cs
# *DO NOT EDIT*

UNREALROOTPATH = /run/media/waimus/SU800/unreal-engine/dev-4.27.2
GAMEPROJECTFILE =/run/media/waimus/SU800/unreal-engine/projects/Rhythmatica/Rhythmatica.uproject

TARGETS = \
	UE4Client-Linux-DebugGame  \
	UE4Client-Linux-Shipping  \
	UE4Client \
	UE4Editor-Linux-DebugGame  \
	UE4Editor-Linux-Shipping  \
	UE4Editor \
	UE4Game-Linux-DebugGame  \
	UE4Game-Linux-Shipping  \
	UE4Game \
	UE4Server-Linux-DebugGame  \
	UE4Server-Linux-Shipping  \
	UE4Server \
	Rhythmatica-Linux-DebugGame  \
	Rhythmatica-Linux-Shipping  \
	Rhythmatica \
	RhythmaticaEditor-Linux-DebugGame  \
	RhythmaticaEditor-Linux-Shipping  \
	RhythmaticaEditor\
	configure

BUILD = bash "$(UNREALROOTPATH)/Engine/Build/BatchFiles/Linux/Build.sh"
PROJECTBUILD = bash "$(UNREALROOTPATH)/Engine/Build/BatchFiles/Linux/RunMono.sh" "$(UNREALROOTPATH)/Engine/Binaries/DotNET/UnrealBuildTool.exe"

all: StandardSet

RequiredTools: CrashReportClient-Linux-Shipping CrashReportClientEditor-Linux-Shipping ShaderCompileWorker UnrealLightmass

StandardSet: RequiredTools UnrealFrontend UE4Editor UnrealInsights

DebugSet: RequiredTools UnrealFrontend-Linux-Debug UE4Editor-Linux-Debug


UE4Client-Linux-DebugGame:
	 $(BUILD) UE4Client Linux DebugGame  $(ARGS)

UE4Client-Linux-Shipping:
	 $(BUILD) UE4Client Linux Shipping  $(ARGS)

UE4Client:
	 $(BUILD) UE4Client Linux Development  $(ARGS)

UE4Editor-Linux-DebugGame:
	 $(BUILD) UE4Editor Linux DebugGame  $(ARGS)

UE4Editor-Linux-Shipping:
	 $(BUILD) UE4Editor Linux Shipping  $(ARGS)

UE4Editor:
	 $(BUILD) UE4Editor Linux Development  $(ARGS)

UE4Game-Linux-DebugGame:
	 $(BUILD) UE4Game Linux DebugGame  $(ARGS)

UE4Game-Linux-Shipping:
	 $(BUILD) UE4Game Linux Shipping  $(ARGS)

UE4Game:
	 $(BUILD) UE4Game Linux Development  $(ARGS)

UE4Server-Linux-DebugGame:
	 $(BUILD) UE4Server Linux DebugGame  $(ARGS)

UE4Server-Linux-Shipping:
	 $(BUILD) UE4Server Linux Shipping  $(ARGS)

UE4Server:
	 $(BUILD) UE4Server Linux Development  $(ARGS)

Rhythmatica-Linux-DebugGame:
	 $(PROJECTBUILD) Rhythmatica Linux DebugGame  -project="$(GAMEPROJECTFILE)" $(ARGS)

Rhythmatica-Linux-Shipping:
	 $(PROJECTBUILD) Rhythmatica Linux Shipping  -project="$(GAMEPROJECTFILE)" $(ARGS)

Rhythmatica:
	 $(PROJECTBUILD) Rhythmatica Linux Development  -project="$(GAMEPROJECTFILE)" $(ARGS)

RhythmaticaEditor-Linux-DebugGame:
	 $(PROJECTBUILD) RhythmaticaEditor Linux DebugGame  -project="$(GAMEPROJECTFILE)" $(ARGS)

RhythmaticaEditor-Linux-Shipping:
	 $(PROJECTBUILD) RhythmaticaEditor Linux Shipping  -project="$(GAMEPROJECTFILE)" $(ARGS)

RhythmaticaEditor:
	 $(PROJECTBUILD) RhythmaticaEditor Linux Development  -project="$(GAMEPROJECTFILE)" $(ARGS)

configure:
	xbuild /property:Configuration=Development /verbosity:quiet /nologo "$(UNREALROOTPATH)/Engine/Source/Programs/UnrealBuildTool/UnrealBuildTool.csproj"
	$(PROJECTBUILD) -projectfiles -project="\"$(GAMEPROJECTFILE)\"" -game -engine 

.PHONY: $(TARGETS)
